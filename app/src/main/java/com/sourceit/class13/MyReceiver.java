package com.sourceit.class13;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import static android.provider.Settings.Global.AIRPLANE_MODE_ON;

public class MyReceiver extends BroadcastReceiver {

    public final int REQ_CODE=101;
    public final int NOTIFY_ID=102;

    public MyReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        //  throw new UnsupportedOperationException("Not yet implemented");

        String action = intent.getAction();
//         if (AIRPLANE_MODE_ON.equals(action)) {

        if (isAirplaneModeOn(context)) {
            sendNotify(context,"Airplane is ON!");
          //  Toast.makeText(context, "Airplane is ON!", Toast.LENGTH_LONG).show();

        } else {
            sendNotify(context,"Airplane is OFF!");
          //  Toast.makeText(context, "Airplane is OFF!", Toast.LENGTH_LONG).show();
//        }

    }}
   // }


    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    AIRPLANE_MODE_ON, 0) != 0;
        }
    }


    void sendNotify(Context context,String message){

        Intent notificationIntent = new Intent(context, MainActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(context,
                REQ_CODE, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentTitle("Airplane mode")
                .setContentText(message);

        Notification n = builder.build();
        nm.notify(NOTIFY_ID, n);
    }


}
